package dam.androidainoa.u2p4conversor;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;



public class MainActivity extends LogActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }
//TODO Ex1 de pulgadas a centimetros y de centimetros a pulgadas
    private void setUI() {
        EditText etPulgada = findViewById(R.id.et_Pulgada);
        EditText etResultado = findViewById(R.id.et_Resultado);
        Button buttonCentimetros = findViewById(R.id.button_Centimetros);
        Button buttonPulgadas = findViewById(R.id.button_Pulgadas);

        buttonCentimetros.setOnClickListener(view -> {
            try {
                etResultado.setText(centimetros(etPulgada.getText().toString()));
            } catch (Exception e) {
                Log.e("LogsCentimetros", e.getMessage());
            }
        });

        buttonPulgadas.setOnClickListener(view -> {
            try {
                etResultado.setText(pulgadas(etPulgada.getText().toString()));
            } catch (Exception e) {
                Log.e("LogsPulgadas", e.getMessage());
            }
        });

    }

    private String centimetros(String pulgadaText) {
        double pulgadaValue = Double.parseDouble(pulgadaText) * 2.54;

        return String.valueOf(pulgadaValue);


    }

    private String pulgadas(String pulgadaText) {
        double centimetroValue = Double.parseDouble(pulgadaText) / 2.54;

        return String.valueOf(centimetroValue);

    }
}