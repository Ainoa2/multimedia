package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends LogActivity {

    public void launchNextActivity(View view){
        startActivity(new Intent(this, NextActivity.class));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupUI();
    }
    private void setupUI(){
        Button btNextActvity=findViewById(R.id.btNextActivity);

        btNextActvity.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startActivity(new Intent(MainActivity.this,NextActivity.class));
            }
        });

    }



}