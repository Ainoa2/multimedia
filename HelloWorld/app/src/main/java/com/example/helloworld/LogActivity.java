package com.example.helloworld;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

public class LogActivity extends AppCompatActivity{

    private static final String DEBUG_TAG="LogActivity";
    private void notify(String eventName){

        String activityName=this.getClass().getSimpleName();

        String CHANNEL_ID="My_LifeCycle";

        if(Build.VERSION.SDK_INT>= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel=new NotificationChannel(CHANNEL_ID,"My Lifecycle",
                    NotificationManager.IMPORTANCE_DEFAULT);

            notificationChannel.setDescription("lifecycle events");
            notificationChannel.setShowBadge(true);

            NotificationManager notificationManager=getSystemService(NotificationManager.class);

            if( notificationManager !=null){
                notificationManager.createNotificationChannel(notificationChannel);
            }

        }
        NotificationManagerCompat notificationManagerCompat=NotificationManagerCompat.from(this);


        NotificationCompat.Builder notificationBuilder= new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(eventName + " " + activityName)
                .setContentText(getPackageName())
                .setAutoCancel(true)
                .setSmallIcon(R.mipmap.ic_launcher);

        notificationManagerCompat.notify((int)System.currentTimeMillis(),notificationBuilder.build());
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onStart() {
        super.onStart();
        int i = 0;
        Log.i(DEBUG_TAG, "LOG-onStart");
        notify("onStart");


    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(DEBUG_TAG, "LOG-onStop");
        notify("onStop");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(DEBUG_TAG, "LOG-onPause");
        notify("onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(DEBUG_TAG, "LOG-onResume");
        notify("onResume");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(DEBUG_TAG, "LOG-onRestart");
        notify("onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(isFinishing()==true) {
            Log.i(DEBUG_TAG, "LOG-isFinishing");
        }else{
            Log.i(DEBUG_TAG, "LOG-isNotFinishing");
        }
        notify("onDestroy");
    }
    }