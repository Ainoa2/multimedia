package dam.ainoaramosochoa.u3t1initialapp;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    private MediaPlayer sonido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Thread timer=new Thread( ){
            @Override
            public void run(){
                try {
                    sleep(5000);
                }catch(InterruptedException e){
                }
                finally{
                    startActivity(new Intent("dam.ainoa.u3t1initialapp.STARTINGPOIN"));
                }
            }
        };
        timer.start();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}