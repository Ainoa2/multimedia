package dam.ainoaramosochoa.u3t1initialapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    private MediaPlayer sonido;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        //TODO ejercicio2
        getSupportActionBar().hide();

        Thread timer=new Thread( ){
            @Override
            public void run(){
                try {
                    sleep(5000);
                }catch(InterruptedException e){
                }
                finally{
                    startActivity(new Intent("dam.ainoa.u3t1initialapp.STARTINGPOIN"));
                }
            }
        };
        timer.start();
    }



    @Override
    protected void onPause(){
        super.onPause();
        finish();
    }
}