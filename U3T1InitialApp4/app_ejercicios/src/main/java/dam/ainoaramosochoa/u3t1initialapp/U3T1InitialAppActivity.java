package dam.ainoaramosochoa.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class U3T1InitialAppActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;


    private TextView tvDisplay;
    private Button buttonIncrease, buttonDecrease,buttonIncreaseTwo, buttonDecreaseTwo, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_app);

        setUI();
    }



    private void setUI() {
        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonIncreaseTwo = findViewById(R.id.buttonIncreaseTwo);
        buttonDecreaseTwo = findViewById(R.id.buttonDecreaseTwo);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonIncreaseTwo.setOnClickListener(this);
        buttonDecreaseTwo.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

//TODO ejercicio1
    @Override
    public void onClick(View view) {


        switch (view.getId()) {
            case R.id.buttonIncrease:
                count++;
                break;
            case R.id.buttonDecrease:
                count--;
                break;
            case R.id.buttonIncreaseTwo:
                count+=2;
                break;
            case R.id.buttonDecreaseTwo:
                count-=2;
                break;
            case R.id.buttonReset:
                count = 0;
                break;
        }

        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);

    }

    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("respuesta", count);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("respuesta",0);
        tvDisplay.setText("Number of Elements: "+ count);
    }






}

