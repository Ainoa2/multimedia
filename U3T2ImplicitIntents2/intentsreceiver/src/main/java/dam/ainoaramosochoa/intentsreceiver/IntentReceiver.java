package dam.ainoaramosochoa.intentsreceiver;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;

import java.text.BreakIterator;

public class IntentReceiver extends AppCompatActivity {
    EditText share;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_receiver);

        share = findViewById(R.id.etText);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if (type.equals("text/plain")) {
                Bundle bundle = intent.getExtras();
                String str = bundle.getString(Intent.EXTRA_TEXT);
                share.setText(str);

            }
        }
    }
}