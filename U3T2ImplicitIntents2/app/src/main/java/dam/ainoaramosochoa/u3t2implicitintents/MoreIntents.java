package dam.ainoaramosochoa.u3t2implicitintents;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class MoreIntents extends AppCompatActivity {
    private EditText etAlarma, etTemporizador, etEvento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_intents);

        Button btAlarma=(Button)findViewById(R.id.btAlarma);
        btAlarma.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                createAlarm("Alarma", 12, 00);
            }
        });

        Button btTemporizador=(Button)findViewById(R.id.btTemporizador);
        btTemporizador.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                startTimer("Temporizador", 30);
            }
        });

        Button btLlamar=(Button)findViewById(R.id.btLlamar);
        btLlamar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                dialPhoneNumber("646509490");
            }
        });

    }

    public void createAlarm(String message, int hour, int minutes) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void startTimer(String message, int seconds) {
        Intent intent = new Intent(AlarmClock.ACTION_SET_TIMER)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_LENGTH, seconds)
                .putExtra(AlarmClock.EXTRA_SKIP_UI, true);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    public void dialPhoneNumber(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
    }
